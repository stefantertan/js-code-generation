require('should')
const validate = require('./generated/validate')

// Required
validate({}).should.not.be.ok()
validate({firstName: 'Peter'}).should.not.be.ok()
validate({lastName: 'Peter'}).should.not.be.ok()

// Names are strings
validate({
  firstName: 1,
  lastName: 'Pan'
}).should.not.be.ok()

validate({
  firstName: true,
  lastName: 'Pan'
}).should.not.be.ok()

validate({
  firstName: 'Peter',
  lastName: 2
}).should.not.be.ok()

validate({
  firstName: 'Peter',
  lastName: false
}).should.not.be.ok()

// Age is number
validate({
  firstName: 'Peter',
  lastName: 'Pan',
  age: 'twelve'
}).should.not.be.ok()

// Age is positive
validate({
  firstName: 'Peter',
  lastName: 'Pan',
  age: -2
}).should.not.be.ok()

// Happy validations
validate({firstName: 'Peter', lastName: 'Pan'}).should.be.ok()
validate({firstName: 'Peter', lastName: 'Pan', age: 12}).should.be.ok()
