const schema = require('./schema.json')
const esformatter = require('esformatter')
const fs = require('fs')

console.log("Generating ./generated/validate.js from ./schema.json\n");
const source = esformatter.format(generateCode(schema))
fs.writeFileSync('./generated/validate.js', source, 'utf-8')

function generateCode (schema) {
  return `module.exports = function validate(obj){

            //Check required properties
            ${schema.required.map(checkRequired).join('\n')}

            //Check types
            ${Object.entries(schema.properties)
              .map(checkType)
              .join('\n')}

            //Check number constraints
            ${Object.entries(schema.properties)
                .map(checkConstraints)
                .join('\n')}

            return true;
          }`
}

function checkType ([name, desc]) {
  return `if(obj.hasOwnProperty('${name}')
  && !(typeof obj.${name} === '${desc.type}')){
    return false;
  }`
}

function checkConstraints ([name, desc]) {
  if (desc.type === 'number' && desc.hasOwnProperty('minimum')) {
    return `if(obj.hasOwnProperty('${name}') && obj.${name} < ${desc.minimum}){
      return false;
    }`
  }
}

function checkRequired (propName) {
  return `if(!obj.hasOwnProperty('${propName}')) {
            return false
          }`
}
